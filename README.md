# GitLabExample

The GitLab counterpart to Julia's Example package: https://github.com/JuliaLang/Example.jl.
GitLabExample is mostly identical to Example, save for the UUID.
This repository is used for testing workflow integration with GitLab-hosted packages.
